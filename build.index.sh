#!/bin/bash

# using flags to inject CI variables

GOOS=js GOARCH=wasm go build \
  -ldflags "-X main.ciProjectId=$CI_PROJECT_ID -X main.ciMergeRequestIid=$CI_MERGE_REQUEST_IID -X main.ciProjectPath=$CI_PROJECT_PATH -X main.ciCommitRefSlug=$CI_COMMIT_REF_SLUG" \
  -o index.wasm

ls -lh *.wasm

